var gulp = require('gulp'),
	plugins = require('gulp-load-plugins')({
		pattern: ['gulp-*', 'gulp.*', 'node-*']
	}),
	_ = require('lodash'),
	path = require('path'),
	config = require('./server/config/config'),
	digitaloceanapi = require('digital-ocean-api'),
    jade = require('gulp-jade'),
	livereload = require('gulp-livereload');

var app = {
	js: ['public/app/**/*.js'],
	html: ['public/app/**/*.html'],
	jade: ['public/app/**/*.jade'],
	scss: ['public/app/**/*.scss'],
	test: ['test/globals.js', 'test/utils.js', 'server/test/**/*.js', 'server/**/test.js']
};

var vendor = {
	js: [
		'bower_components/angular/angular.js',
		'bower_components/angular-ui-router/release/angular-ui-router.js',
		'bower_components/angular-bootstrap/ui-bootstrap-tpls.js',
		'bower_components/jquery/dist/jquery.js',
		'bower_components/bootstrap/dist/js/bootstrap.js',
		'bower_components/lodash/dist/lodash..js',
		'bower_components/modernizr/modernizr.js',
		'bower_components/moment/moment.js'
	],
	css: [
		'bower_components/bootstrap/dist/css/bootstrap.css',
		'bower_components/bootstrap/dist/css/bootstrap-theme.css',
		'bower_components/fontawesome/css/font-awesome.css',
		'public/assets/fonts/fonts.css'
	],
	fonts: [
		'bower_components/bootstrap/dist/fonts/*',
		'bower_components/fontawesome/fonts/*'
	]
};

var notify = function (msg) {
	console.log(msg);
	plugins.nodeNotifier.notify({
		title: config.app.title || 'Notification',
		message:msg
	});
};

var onEnd = function (msg) {
	console.log(msg);
	return function () {
		notify(msg);
	}
};

gulp.task('reload', function(){
	livereload.reload();
});


gulp.task('vendorJS', function () {
	return gulp.src(vendor.js)
		.pipe(plugins.concat('vendor.bundle.js'))
		.pipe(plugins.bytediff.start())
		.pipe(plugins.if(process.env.NODE_ENV == 'prod', plugins.uglify()))
		.pipe(plugins.bytediff.stop())
		.pipe(gulp.dest('public/assets/js'))
		.on('end', onEnd('Vendor JS compiled!'))
});

gulp.task('vendorCSS', function () {
	return gulp.src(vendor.css)
		.pipe(plugins.concat('vendor.bundle.css'))
		.pipe(plugins.bytediff.start())
		.pipe(plugins.minifyCss())
		.pipe(plugins.bytediff.stop())
		.pipe(gulp.dest('public/assets/css'))
		.on('end', onEnd('Vendor CSS compiled!'))
});

gulp.task('appJS', function () {
	return gulp.src(app.js)
		.pipe(plugins.ngAnnotate()).on('error', function (err) {
			console.log('Annotation error',err.message);
		})
		.pipe(plugins.angularFilesort())
		.pipe(plugins.concat('app.bundle.js'))
		.pipe(plugins.bytediff.start())
		.pipe(plugins.if(process.env.NODE_ENV == 'prod', plugins.uglify()))
		.pipe(plugins.bytediff.stop())
		.pipe(gulp.dest('public/assets/js'))
		.on('end', onEnd('App JS compiled!'))
});


gulp.task('appSCSS', function () {
	return gulp.src(app.scss)
		.pipe(plugins.concat('app.bundle.css'))
		.pipe(plugins.sass())
		.pipe(plugins.bytediff.start())
		.pipe(plugins.minifyCss())
		.pipe(plugins.bytediff.stop())
		.pipe(gulp.dest('public/assets/css'))
		.on('end', onEnd('App SCSS compiled!'));
});


gulp.task('appHTML', function () {

	return gulp.src(['public/app/core/*.jade', 'public/app/login/*.jade', 'public/app/register/*.jade'])
		.pipe(jade({
			basedir: __dirname,
			pretty: true
		}))
		.pipe(gulp.dest('public/html'))
		.on('end', onEnd('Jade files compiled successfully!'));

});


gulp.task('app', ['appJS', 'appHTML', 'appSCSS']);
gulp.task('vendor', ['vendorJS', 'vendorCSS']);

gulp.task('fonts', function () {
	return gulp.src(vendor.fonts)
		.pipe(gulp.dest('public/assets/fonts'));
});

gulp.task('build', ['fonts', 'app', 'vendor']);

gulp.task('test', function () {
	return gulp.src(app.test, {read: false})
		.pipe(plugins.mocha({
			reporter: 'mocha-unfunk-reporter',
			slow: 15
		}))
		.on('error', function (err) {
			plugins.nodeNotifier.notify({
				title: 'Mocha',
				message: err.message,
				sound: true
			});
			console.log('Error stack', err.stack);
		});
});


gulp.task('watch:test', function () {
	gulp.watch(['server/**/*.js'], ['test']);
});

gulp.task('watch:index', function() {
	gulp.watch(['public/index.html'], ['reload']);
	gulp.watch(['public/assets/css/*.css'], ['reload']);
	gulp.watch(['public/assets/js/*.js'], ['reload']);
});

gulp.task('watch:app', function () {
	gulp.watch(app.js, ['appJS']);
	gulp.watch(app.jade, ['appHTML']);
	gulp.watch(app.scss, ['appSCSS']);
});

gulp.task('watch', ['watch:app', 'watch:test', 'watch:index'], function () {
	plugins.nodemon({
		script: 'server.js',
		watch: ['server', 'server.js'],
		env: {
			'NODE_ENV': 'dev'
		}
	})
		.on('stderr', function (err) {
			console.log('Got error');
			console.log(err);
		});
	livereload.listen();
	notify('Watching for changes...');
});

gulp.task('default', ['watch']);
