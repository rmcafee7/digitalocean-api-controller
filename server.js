var express = require('express'),
	chalk = require('chalk'),
	config = require('./server/config/config'),
	debug = require('debug')('app:main');

var https = require('https');
var http = require('http');
var app = express();

require('./server/init.js')(app);

http.createServer(app).listen(config.http_port, function(){
	console.log('SkynetApp Init complete');
});

//https.createServer(options, app).listen(config.https_port, function(){
//	console.log('SkynetApp Init complete');
//});
