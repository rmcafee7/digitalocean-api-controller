module.exports = {
	app: {
		title: 'SkynetApp'
	},
	http_port: process.env.PORT || 80,
	https_port: process.env.PORT || 443,
	secure: process.env.SECURE || false,
	connectionPool: 1000,
	api: {
		enabled: true,
		version: 'v1',
		path: '/api'
	},
	auth: {
		saltWorkFactor: 10,
		secret: 'd63568e672bc25e6c951df4c4d94d61c'
	},
	sessionSecret: '552cbc0a24bd78b9615be0145936a879',
	sessionCollection: 'sessions',
	enable: {
		registrations: false
	},
	mail: {
		apiKey: 'LmKb69XGtG76TgmQnL41Qg',
		debug: {
			email: [{
				email: 'admin@ryanmcafee.com',
				name: 'Debug'
			}]
		}
	}
};
